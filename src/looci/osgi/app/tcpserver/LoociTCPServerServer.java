/**
LooCI Copyright (C) 2013 KU Leuven.
All rights reserved.

LooCI is an open-source software development kit for developing and maintaining networked embedded applications;
it is distributed under a dual-use software license model:

1. Non-commercial use:
Non-Profits, Academic Institutions, and Private Individuals can redistribute and/or modify LooCI code under the terms of the GNU General Public License version 3, as published by the Free Software Foundation
(http://www.gnu.org/licenses/gpl.html).

2. Commercial use:
In order to apply LooCI in commercial code, a dedicated software license must be negotiated with KU Leuven Research & Development.

Contact information:
  Administrative Contact: Sam Michiels, sam.michiels@cs.kuleuven.be
  Technical Contact:           Danny Hughes, danny.hughes@cs.kuleuven.be
Address:
  iMinds-DistriNet, KU Leuven
  Celestijnenlaan 200A - PB 2402,
  B-3001 Leuven,
  BELGIUM. 
 */
package looci.osgi.app.tcpserver;

import java.io.*;
import java.net.*;

public class LoociTCPServerServer implements Runnable {

	private int port;
	private ServerSocket servSocket;
	private static Socket socket;
	private LoociTCPServerComponent comp;
	private static BufferedReader in;
	private static PrintWriter out;
	private static boolean flag;
	private static StringBuffer toSend = new StringBuffer("");
	private static String s;
	
	public LoociTCPServerServer(int port, LoociTCPServerComponent component) {
		
		System.out.println("[SERVERSERVER] created");
		this.port = port;
		this.comp = component;
		flag = false;
		//connect();
	}

	public void connect() {

		try {

			servSocket = new ServerSocket(port);
			socket = servSocket.accept();
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			cleanUp();
		}
		System.out.println("[SERVERSERVER] connected");
	}

	public void run() {

		connect();

		while(!flag) {
			
			//System.out.println("[SERVERSERVER]LoociTCPServerServer is running");
			
			//try {
				
				if (toSend.length() !=0) {
					
					System.out.println("[SERVERSERVER] Sending out: " + toSend.toString());
					out.print(toSend);
					out.flush();
					toSend.setLength(0);
				}

				/*if (in.ready()) {
					
					System.out.println("In is ready!!!!");
					s=in.readLine();
					if ((s!=null)&&(s.length()!=0)) {
						// do your thing!!
						System.out.println("Received: " + s);
					}
				}
			} catch (IOException e) {
				cleanUp();
			}
			*/

			wait(10);
		}
	}

	private void wait(int time) {
		
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			cleanUp();
		}
	}

	private static void cleanUp() {
		
		System.out.println("Cleaning up TCPServer");
		flag = true;
		try {
			if (socket != null) {

				socket.close();
				socket = null;
			}
		} catch (IOException e) {
			socket = null;
		}

		try {
			if (in != null) {

				in.close();
				in = null;
			}
		} catch (IOException e) {
			in = null;
		}

		if (out != null) {
			
			out.close();
			out = null;
		}
	}

	public void setToSend(String data) {
		
		toSend.append(data);
	}
}
